UW-Madison API Standards
========================

This document describes standards each UW API must meet to be considered for inclusion in the Developer Portal ([https://developer.wisc.edu](https://developer.wisc.edu))

These standards are maintained by the Enterprise Integration API Team with consultation and support from other API producers and stakeholders.
Please contact [api@doit.wisc.edu](mailto:api@doit.wisc.edu) with questions and feedback.

Motivation
----------

*   UW-Madison has an overarching data documentation and integration standards, both of which leave room for API-specific standards to be curated by DoIT.

    *   [Institutional Data Documentation Standard](https://uwmadison.app.box.com/s/xvi4mqicuiiezoqi6njywi3cow63z65h)
        
    *   [Institutional Data Integration Standard](https://uwmadison.app.box.com/s/wla5zksaeab8wk299uo69qqjzfevzg7c)
        
    *   See also: [Policies, Procedures, and Standards](https://data.wisc.edu/data-governance/policies-procedures/)
        
*   Each API in the Developer Portal should conform to a minimum standard. It is important that we provide a consistent user experience, data, security, and authorization practices across UW APIs.

*   APIs and their documentation should be easy to find, understand, and maintain. Lowering the barrier to using APIs through good, consistent documentation will promote adoption of APIs, reduce support burdens on API producers, and will increase likelihood of integrator success.
    

API Standard
------------

*   If an integrator has a reasonable expectation of being approved for an API that they need for a valid business reason or integration, then that API is likely to be a good candidate for inclusion in the Developer Portal.

*   If an API is tailored to a specific use case or expresses data unique to a particular consuming system, it is likely not appropriate to include in the Developer Portal.

*   If an API’s purpose is narrow in scope, but might provide value to many consumers, irrespective of adoption, it may be considered broadly useful.

*   **Abstracted:** APIs that express institutional data as defined by the [Data Governance Council](https://data.wisc.edu/data-governance/data-domains-trustees-and-stewards/) should present appropriate domain data models and not vendor-specific terminology.
    
*   **Naming:** The name of an API presented in the Developer Portal should also reflect abstraction, avoiding vendor-specific naming where possible. If acronyms are necessary in the API name, they should be spelled out and explained in the documentation.
    
*   **Mock and Test:** API Producers are encouraged to provide testing functionality with sample data that do not require an approval process. APIs that present sample data should be prefaced with the word “Mock”. Test versions of APIs with test data may also be presented in the Developer Portal, may require authorization, and may be separate than Mock APIs.

*   **Risk management:** Each API must comply with UW-Madison’s Cybersecurity risk management framework as [outlined here](https://it.wisc.edu/about/division-of-information-technology/enterprise-information-security-services/office-of-cybersecurity/risk-management-and-compliance-rmc/).In practice, this usually means undergoing a Cybersecurity Consultative Review (CCR) to assess, manage, and mitigate risks.

*   **Well documented:** Each API must have up-to-date documentation that enables API consumers to easily get started and use the API. This documentation must conform to the requirements outlined below.

*   **Coordination of Boundaries:** APIs that express data across multiple domain boundaries should be coordinated, data should be expressed in alignment with institutional models, and that data should be sourced from an appropriate system of record or reference (see example below).

### Example of Coordination of Boundaries

If a Curricular and Academic API needs to present Person data, then API Producer teams should coordinate whether the Curricular and Academic API will reference the Person API in its responses (i.e.hand off) or if data is presented directly using common data models.

If the Curricular and Academic API expresses Person data directly, it might use a near real-time mechanism like webhooks to update that data from the Person API to allow data to remain synchronized in both APIs. The data should also be presented using a common data model as defined by each specification.

Similarly, if the Curricular and Academic API expresses data from the Person API, both should have similar classification and authorization processes, as defined by data governance. Data restricted from a given application using the Person API should not be accessible through the Curricular and Academic API.

Documentation Standard
----------------------

### Presentation

*   **Cross-linking:** Documentation should either be housed within or linked from within the Developer Portal and accessible to the same population of integrators without additional authorization.

*   **Visuals:** APIs should not have logos or other marketing-style material since it is challenging to synchronize style across disparate API producer teams.

### Required

The topics below are required documentation for each API, and producers must actively maintain this documentation for an API to be included in the Developer Portal.

*   **Getting Access**

    *   Authorization workflow, timeline, and expectations

    *   Terms of use

*   **Specification**

    *   OpenAPI/Swagger for RESTful APIs

        *   Documentation must be linked from the info.description section so it can be found from within other tools like Postman and IDEs

    *   Data dictionary (or external link if applicable)

    *   Data classification(s)

*   **Integrator Guidelines**

    *   Audience for the API

    *   Example use cases for the API

    *   Error management practices

        *   Error behaviors and definitions

        *   Retry and backoff guidance

    *   How to perform or simulate

        *   End-to-end testing

        *   Load testing

*   **Operations**

    *   Service level agreements (SLA) or objectives (SLO) and related metrics

    *   Versioning strategy

    *   Rate limits and throttling

    *   Release notes

    *   How to obtain support


### Recommended

The API Team also recommends documenting the supporting topics below, but these are not strictly required for inclusion in the Developer Portal.

*   **Tutorials**

    *   Using the API with tools like Postman

    *   Postman collections or configurations for other tooling

    *   How to use JSON:API includes, filters, pagination, webhooks, etc.

    *   Transcript or text version of video tutorials

*   **Developer Support**

    *   Templates or code snippets

    *   Clients (where applicable and actively maintained)

    *   Community supported or external resources

    *   Troubleshooting guides

    *   Related APIs and how to work with data across multiple APIs