# Apigee

"Apigee is a platform for developing and managing APIs. By fronting services with a proxy layer, Apigee provides an abstraction or facade for your backend service APIs and provides security, rate limiting, quotas, analytics, and more."

https://cloud.google.com/apigee/docs/api-platform/get-started/what-apigee

Apigee is the API Management platform for managing institutional APIs at UW-Madison.

[[_TOC_]]

## Apigee Training

We currently have access to a limited number of licenses for Apigee training through [Coursera](https://www.coursera.org). This training, which is a mix of lectures and hands-on labs, provides an introduction to the API publishing capabilities available within the Apigee platform.

To request to use a license [contact the API Team](mailto:api@doit.wisc.edu). Upon review of your request and if a license is available, you will receive an email from Coursera with a link to join the Apigee training program for UW-Madison.

## Getting Access to Publish APIs

Apigee is meant for teams that want to publish APIs for others to use.
When others browse APIs and get access to use them, they will do so through a Developer Portal instead of interacting with Apigee directly.

_The API Team is currently onboarding publishers to Apigee on a limited basis._

This is due to the low level of isolation that exists within Apigee.  While Apigee provides granular control over role-based permissions, it does not provide a means for separating access so that multiple teams can exist in the same Apigee organization without being able to access the other's APIs and related resources.

Publishers that choose to use Apigee to publish and manage their API should be aware of these limitations:

- All API publishers will have the same access to the development, pre-production, and production Apigee organizations.
- All API publishers will have access to view and edit all API proxies, API products, and API documentation published to the developer portal.
- All API publishers will be able to view and edit developer applications subscribed to their APIs or anyone else's APIs. _This includes viewing OAuth2 keys and secrets associated with a developer application._
- All API publishers will be able to view and modify information used to connect to backend services (the actual API that the Apigee API proxy is sending traffic to) _including credentials and secrets._

If these limitations *do not* meet the needs of your use-case, please [contact the API Team](mailto:api@doit.wisc.edu) to give us specific feedback and to stay updated when Apigee can better handle multiple teams coexisting in the same Apigee organization.

If these limitations *do* meet the needs of your use case, please follow the instructions below to request access to Apigee:

1. Contact the API Team (api@doit.wisc.edu) and include your name, the team/department you're representing, and an overview of your use-case for using Apigee. Someone will be in contact with you between 1-3 business days to gather more information or schedule a call to discuss further.
2. After confirming the API Team is able to onboard your team to Apigee, [create a Google Group](https://kb.wisc.edu/googleapps/page.php?id=98121) that contains the team members that you would like to allow access to Apigee. Send the name of the group (e.g. my-team@g-groups.wisc.edu) to the API Team. This group will be controlled by your team and is used to provide or remove your team's access to Apigee. Note, your team may only consist of yourself, but it is still beneficial to have access controlled through a group in case API ownership needs to be transferred in the future.

## Login to Apigee

Login to Apigee here: https://console.cloud.google.com/apigee

Apigee uses Google and NetID login for authentication. Make sure you are logged into Google using your NetID.

If prompted, make sure you are using the project `doit-ipt-apigee-dev-808d`

## Onboarding a New API

1. Select `doit` environment to deploy an API proxy
    * UW-Madison currently has a limited licensing of 3 Apigee organizations (development, pre-prod, prod), and 15 Environments across all Apigee organizations. If new environments or any changes are needed, please contact the API Team (api@doit.wisc.edu) to create/edit environments.
1. [Create a reverse proxy for your API](https://cloud.google.com/apigee/docs/api-platform/fundamentals/understanding-apis-and-api-proxies)
    * A reverse proxy can also be created using the VS Code Apigee plugin as described in
      [Developing API proxies](https://cloud.google.com/apigee/docs/api-platform/local-development/vscode/develop-apiproxies)
    * If your API is IP restricted, add these IP addresses to your firewall's allow-list for each Apigee organization.
        * dev: 34.71.118.160
        * preprod: 34.134.105.239
        * prod: 35.238.42.232

1. [Publish your API](https://cloud.google.com/apigee/docs/api-platform/publish/publishing-overview).

## Authorizing Calls to Your API (OAuth2)

[OAuth2](https://cloud.google.com/apigee/docs/api-platform/security/oauth/oauth-introduction) is the preferred method for authorizing calls to APIs deployed in Apigee.
The API Team maintains an Apigee proxy that can be used to get an access token to use for authorizing calls to other APIs.
API publishers do not need to create their own OAuth2 proxy, but they do need to configure their proxies to check the OAuth2 access token before proceeding with the rest of the request.
More information on this process can be found in the [OAuthV2 policy documentation](https://cloud.google.com/apigee/docs/api-platform/reference/policies/oauthv2-policy#verifyaccesstoken).

The API specification for the OAuth proxy (i.e. for getting an access token) can be [found on the Developer Portal](https://developer.wisc.edu/docs/oauth/1/overview).

### Limitations

* Access tokens issues by the OAuth proxy can only be used with APIs in the same Apigee organization. For example, a token issued in the dev organization cannot be used in the production org, and vice-versa.
* The OAuth proxy currently only supports the Client Credentials grant type. If you have a use-case that requires other grant types, contact the API Team (api@doit.wisc.edu) for further discussion.

## Developer Portal

The Developer Portal is where API consumers go to register for and view documentation about APIs.
Unlike API publishers, consumers do not interact directly with the Apigee console.
Apigee is a backend implementation detail of the overall experience of discovering, registering for, and using an API.

Each Apigee organization has its own Developer Portal.
Non-production Developer Portals should only be used for API teams to test their own APIs, as well as for getting feedback from stakeholders on APIs before they are released in production.
Otherwise, the [production Developer Portal](https://developer.wisc.edu/) should be the only location where API consumers discover and register for APIs.

![API development vs. production environments](./static/dev-prod-domains.png)

If your API requires additional environments to be able to satisfy the use-cases of your API (i.e. in addition to the mock and non-mock environments in the production Apigee organization), please [contact the API Team](mailto:api@doit.wisc.edu) so that we can discuss your use-case.
Separate Apigee organizations, and therefore separate Developer Portals, should not be used as a substitute for additional environments.

### Allowing Developer Portal Access to Your API

In order for consumers to call your API from the developer portal, you need enable the developer portal CORS policy:

* Create a new [Flow Callout policy](https://cloud.google.com/apigee/docs/api-platform/reference/policies/flow-callout-policy) in your API proxy that uses the `SF_CORS_DEVPORTAL` shared flow.
* Add this policy to the request pre-flow of your API proxy.

## Using GitLab Runner to Publish APIs

The API Team can create a Gitlab runner for publishing APIs using Gitlab's CI/CD pipelines. Your Gitlab runner will only
be able to modify Apigee resources that begin with your team's assigned prefix.  If you are interested in using this in
your own projects, [contact the API Team](mailto:api@doit.wisc.edu) with the following information to have this runner
enabled for your GitLab workspace.

1. Your preferred team prefix:
    * 10 characters or fewer
    * Only lowercase letters, numbers, and hyphens
    * Starts with an alphabetic character
    * Ends with an alphabetic or numeric character
    * For example: "api-team" or "asp-api"

2. A shared secret for your GitLab Runner token:
    * From the workspace in GitLab where you want to use the runner, go to CI/CD > Runners and click the
      "Register a group runner" button to access the token.
    * Put this token in a secure location and send us that location in your email.

## Logging in Apigee

### [API Monitoring](https://cloud.google.com/apigee/docs/api-monitoring)

After you build and launch your APIs on Apigee, you need to ensure that they are available and performing as expected in
order to maintain uninterrupted service. Apigee's API Monitoring enables you to track your APIs to make sure they are up
and running correctly. API Monitoring provides near real-time insights into API traffic and performance, to help you
quickly diagnose and solve issues as they arise.

### [Developer Engagement](https://cloud.google.com/apigee/docs/api-platform/analytics/partner-engagement-dashboard)

The Developer Engagement dashboard tells you which of your registered app developers are generating the most API traffic.
For each of your developers, you can find out who is generating the most API traffic and the most errors. For example,
if a particular developer's app is generating a lot of errors relative to other developers, you can pro-actively address
the problem with that developer.

### Proxy Usage Logs

Detailed information about who requested what data from each proxy is logged in GCP.  These logs are accessed through the
[Log Explorer](https://cloud.google.com/logging/docs/view/logs-explorer-interface) by searching for the log name
`apigee-proxy-usage`.  To look at only logs for your proxy, press the `Show Query` toggle and add the filter
`jsonPayload.proxy={proxy-name}`.

To filter logs based on elapsed time, add the filter `jsonPayload.time_elapsed>value` or `jsonPayload.time_elapsed<value`.
([Supported comparison operators](https://cloud.google.com/logging/docs/view/logging-query-language#comparison_operators))

![Proxy Usage Logs in GCP](static/Apigee Proxy Usage Logs.png "Proxy Usage Logs in GCP")