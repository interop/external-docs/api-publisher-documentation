# API Publisher Documentation

This repository contains documentation for API Publishers who create institutional APIs for the UW Developer Portal (https://developer.wisc.edu).

[[_TOC_]]

## UW-Madison API Standards

All APIs must meet a set of requirements in order to be published on the UW Developer Portal.
See [this document](./api-standards.md) for details.

## Apigee Documentation

Google Apigee is an API Management platform used for managing access to institutional APIs.
See [the documentation](./apigee.md) for details and steps to get access.

## Feedback and questions

Changes to the documentation in this repository are welcome.
Please create a merge request with your suggested changes.
For any other feedback or questions, please contact the DoIT Enterprise Integration API Team: api@doit.wisc.edu
